# Overview  

![overview](./overview.svg)

# Kafka  

## Create Topics  

    /opt/bitnami/kafka/bin/kafka-topics.sh --create --topic matcher --partitions 1 --replication-factor 1 --bootstrap-server 127.0.0.1:9092
    /opt/bitnami/kafka/bin/kafka-topics.sh --create --topic ticker --partitions 1 --replication-factor 1 --bootstrap-server 127.0.0.1:9092

# DynamoDB  

## Create Tables  

    aws dynamodb delete-table --table-name ticker --endpoint-url http://localhost:8000
    aws dynamodb create-table --cli-input-json file://C:\dev\projects\kafka-spark-quarkus-football-ticker\src\main\resources\dynamodb-ticker.json --endpoint-url http://localhost:8000
    aws dynamodb update-time-to-live --table-name ticker --time-to-live-specification "Enabled=true, AttributeName=ttl" --endpoint-url http://localhost:8000

## DynamoDB Admin  

    npm install -g dynamodb-admin
    set DYNAMO_ENDPOINT=http://localhost:8000
    dynamodb-admin

## UIs

Webapp: http://localhost:8080  
DynamoDB-Admin: http://localhost:8001

Artemis: http://localhost:8161/console/auth/login  
user: artemis - password: simetraehcapa

S3: http://localhost:9000/ui