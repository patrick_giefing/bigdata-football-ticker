package at.ac.fhwn.bigdata.footballticker.service

import at.ac.fhwn.bigdata.footballticker.config.AWS_REGION
import at.ac.fhwn.bigdata.footballticker.config.S3_ENDPOINT
import at.ac.fhwn.bigdata.footballticker.config.createJacksonMapper
import at.ac.fhwn.bigdata.footballticker.entity.input.Match
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import software.amazon.awssdk.core.sync.RequestBody
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.BucketAlreadyExistsException
import software.amazon.awssdk.services.s3.model.BucketAlreadyOwnedByYouException
import software.amazon.awssdk.services.s3.model.CreateBucketRequest
import software.amazon.awssdk.services.s3.model.PutObjectRequest
import java.net.URI
import javax.ws.rs.core.MediaType

const val BUCKET_MATCHES = "matches"

@Service
class S3Service {
    private val logger: Logger = LoggerFactory.getLogger(SparkTickerAggregator::class.qualifiedName)

    fun saveMatch(match: Match) {
        try {
            val s3 = S3Client.builder().endpointOverride(URI(S3_ENDPOINT)).region(Region.of(AWS_REGION))
                .build() // TODO .withPathStyleAccessEnabled(true)
            /*val s3 = AmazonS3ClientBuilder.standard()
                .withEndpointConfiguration(AwsClientBuilder.EndpointConfiguration(S3_ENDPOINT, AWS_REGION))
                .withPathStyleAccessEnabled(true)
                .build()

            if (!s3.doesBucketExistV2(BUCKET_MATCHES)) {
                s3.createBucket(BUCKET_MATCHES)
            }

            val bais = ByteArrayInputStream(createJacksonMapper().writeValueAsBytes(match))
            val putObjectRequest = PutObjectRequest(
                BUCKET_MATCHES,
                "${match.matchInfo.matchId}.json",
                bais, ObjectMetadata()
            )
            s3.putObject(putObjectRequest)*/

            try {
                s3.createBucket(CreateBucketRequest.builder().bucket(BUCKET_MATCHES).build())
            } catch (ex: BucketAlreadyExistsException) {
            } catch (ex: BucketAlreadyOwnedByYouException) {
            }

            s3.putObject(
                PutObjectRequest.builder()
                    .bucket(BUCKET_MATCHES)
                    .key("${match.matchInfo.matchId}.json")
                    .contentType(MediaType.APPLICATION_JSON)
                    .build(),
                RequestBody.fromBytes(createJacksonMapper().writeValueAsBytes(match))
            )
        } catch (ex: Exception) {
            logger.error("Error writing to S3", ex)
        }
    }
}