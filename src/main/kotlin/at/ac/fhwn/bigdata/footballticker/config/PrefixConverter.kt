package at.ac.fhwn.bigdata.footballticker.config

import at.ac.fhwn.bigdata.footballticker.entity.input.MatchEventAggregationPeriodEvent
import software.amazon.awssdk.enhanced.dynamodb.AttributeConverter
import software.amazon.awssdk.enhanced.dynamodb.AttributeValueType
import software.amazon.awssdk.enhanced.dynamodb.EnhancedType
import software.amazon.awssdk.services.dynamodb.model.AttributeValue

const val PREFIX_MATCH = "match"
const val PREFIX_MESSAGE = "message"
const val PREFIX_PERIOD_EVENT = "periodEvent"

/*class LocalDateTimeConverter : DynamoDBTypeConverter<String?, LocalDateTime?> {
    override fun convert(dateTime: LocalDateTime?): String? {
        return dateTime?.toString()
    }

    override fun unconvert(s: String?): LocalDateTime? {
        return s?.let { LocalDateTime.parse(it) }
    }
}*/

abstract class PrefixConverter(private val prefix: String) : AttributeConverter<String> {
    override fun transformFrom(key: String): AttributeValue =
        AttributeValue.builder().s("${prefix}_${key}").build()

    override fun transformTo(value: AttributeValue): String = value.s().split("_").last()

    override fun type(): EnhancedType<String> = EnhancedType.of(String::class.java)

    override fun attributeValueType(): AttributeValueType = AttributeValueType.S
}

class MatchPrefixConverter : PrefixConverter(PREFIX_MATCH) {}

class MessagePrefixConverter : PrefixConverter(PREFIX_MESSAGE) {}

class PeriodEventConverter : AttributeConverter<MatchEventAggregationPeriodEvent> {
    override fun transformFrom(periodEvent: MatchEventAggregationPeriodEvent): AttributeValue =
        AttributeValue.builder()
            .s("${PREFIX_PERIOD_EVENT}_${periodEvent.period ?: 0}_${periodEvent.eventType ?: "info"}").build()

    override fun transformTo(value: AttributeValue): MatchEventAggregationPeriodEvent {
        val parts = value.s().split("_")
        return MatchEventAggregationPeriodEvent(parts[1].toInt(), parts[2])
    }

    override fun type(): EnhancedType<MatchEventAggregationPeriodEvent> =
        EnhancedType.of(MatchEventAggregationPeriodEvent::class.java)

    override fun attributeValueType(): AttributeValueType = AttributeValueType.S
}