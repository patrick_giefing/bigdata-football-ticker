package at.ac.fhwn.bigdata.footballticker.repo

import at.ac.fhwn.bigdata.footballticker.config.*
import at.ac.fhwn.bigdata.footballticker.entity.input.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository
import software.amazon.awssdk.enhanced.dynamodb.Key
import software.amazon.awssdk.enhanced.dynamodb.TableSchema
import software.amazon.awssdk.enhanced.dynamodb.model.BatchWriteItemEnhancedRequest
import software.amazon.awssdk.enhanced.dynamodb.model.QueryConditional
import software.amazon.awssdk.enhanced.dynamodb.model.QueryEnhancedRequest
import software.amazon.awssdk.enhanced.dynamodb.model.WriteBatch
import software.amazon.awssdk.services.dynamodb.model.AttributeValue
import java.io.Serializable
import java.util.function.Consumer
import java.util.stream.Collectors
import javax.annotation.PostConstruct


abstract class DynamoDbRepo<T : DynamoDbMatchEntity> constructor(
    private val partitionKeyPrefix: String,
    private val clazz: Class<T>,
    private val tableName: String
) : Serializable {
    fun schema() = TableSchema.fromBean(clazz)
    fun table() = dynamoDbEnhancedClient().table(tableName, schema())

    open fun save(entity: T) {
        setTtlIfNotSet(entity)
        table().putItem(entity)
    }

    open fun batchSave(entities: Iterable<T>) {
        entities.forEach(::setTtlIfNotSet)
        batch {
            entities.forEach { entity -> it.addPutItem { r -> r.item(entity) } }
        }
    }

    open fun batchDelete(entities: Iterable<T>) {
        entities.forEach { table().deleteItem(it) }
    }

    private fun batch(consumer: Consumer<WriteBatch.Builder<T>>) {
        dynamoDbEnhancedClient().batchWriteItem(
            BatchWriteItemEnhancedRequest.builder()
                .writeBatches(
                    WriteBatch.builder(clazz)
                        .mappedTableResource(table())
                        .apply {
                            consumer.accept(this)
                        }
                        .build())
                .build())
    }

    open fun findByEntitiesName(entityName: String, clazz: Class<T>): List<T> {
        val index = table().index(INDEX_ENTITY_NAME)
        val resultPages = index.query(
            QueryEnhancedRequest.builder()
                .queryConditional(
                    QueryConditional
                        .keyEqualTo(
                            Key.builder().partitionValue(AttributeValue.builder().s(entityName).build())
                                .build()
                        )
                )
                .build()
        )
        val results = resultPages.stream().flatMap { it.items().stream() }.collect(Collectors.toList())
        return results
    }

    fun findByPartitionKeyAndSortKeyPrefix(partitionKey: String, sortKeyPrefix: String): List<T> {
        val resultPages = table().query(
            QueryEnhancedRequest.builder()
                .queryConditional(
                    QueryConditional.sortBeginsWith(
                        Key.builder().partitionValue(partitionKey).sortValue(sortKeyPrefix).build()
                    )
                ).consistentRead(false).build()
        )
        val results = resultPages.stream().flatMap { it.items().stream() }.collect(Collectors.toList())
        return results
    }
}

@Repository
class MatchInfoRepo @Autowired constructor(
    private val matchEventAggregationRepo: MatchEventAggregationRepo,
    private val timelineInfoRepo: TimelineInfoRepo
) : DynamoDbRepo<MatchInfo>(PREFIX_MATCH, MatchInfo::class.java, TABLE_TICKER) {
    fun findAllMatchInfos(): List<MatchInfo> {
        return findByEntitiesName(ENTITY_NAME_MATCH_INFO, MatchInfo::class.java)
    }

    fun findMatchInfo(matchId: String): MatchInfo? {
        return findByPartitionKeyAndSortKeyPrefix(matchId, PREFIX_MATCH).firstOrNull()
    }

    fun deleteAllMatchInfos() {
        findAllMatchInfos().forEach { table().deleteItem(it) } // TODO batch
    }

    fun deleteMatch(matchId: String) {
        val matchKey = "${PREFIX_MATCH}_${matchId}"
        findMatchInfo(matchKey)?.let { table().deleteItem(it) }
        // TODO batch
        matchEventAggregationRepo.let { repo ->
            repo.findMatchEventAggregations(matchKey).forEach { repo.table().deleteItem(it) }
        }
        timelineInfoRepo.let { repo ->
            repo.findTimelineInfos(matchKey).forEach { repo.table().deleteItem(it) }
        }
    }
}

lateinit var matchEventAggregationRepoInstance: MatchEventAggregationRepo

@Repository
class MatchEventAggregationRepo : DynamoDbRepo<MatchEventAggregation>(
    PREFIX_MATCH,
    MatchEventAggregation::class.java,
    TABLE_TICKER
) {
    @PostConstruct
    fun created() {
        matchEventAggregationRepoInstance = this
    }

    fun findAllMatchEventAggregations(): List<MatchEventAggregation> {
        return findByEntitiesName(ENTITY_NAME_MATCH_AGGREGATION, MatchEventAggregation::class.java)
    }

    fun findMatchEventAggregations(matchId: String): List<MatchEventAggregation> {
        return findByPartitionKeyAndSortKeyPrefix(matchId, PREFIX_PERIOD_EVENT)
    }
}

@Repository
class TimelineInfoRepo :
    DynamoDbRepo<TimelineInfo>(PREFIX_MATCH, TimelineInfo::class.java, TABLE_TICKER) {
    fun findAllTimelineInfos(): List<TimelineInfo> {
        return findByEntitiesName(ENTITY_NAME_MATCH_TICKER_MESSAGE, TimelineInfo::class.java)
    }

    fun findTimelineInfos(matchId: String): List<TimelineInfo> {
        return findByPartitionKeyAndSortKeyPrefix(matchId, PREFIX_MESSAGE)
    }
}

fun setTtlIfNotSet(entity: DynamoDbMatchEntity) {
    if (entity.ttl == null) {
        entity.ttl = (System.currentTimeMillis() / 1000) + 150
    }
}