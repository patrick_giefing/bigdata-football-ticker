package at.ac.fhwn.bigdata.footballticker.entity.input

import at.ac.fhwn.bigdata.footballticker.config.MatchPrefixConverter
import com.fasterxml.jackson.annotation.JsonAlias
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.*
import java.time.LocalDate
import java.time.LocalDateTime

interface DynamoDbEntity {
    @get:JsonIgnore
    @get:DynamoDbSecondaryPartitionKey(indexNames = [INDEX_ENTITY_NAME])
    var entityName: String
}

interface DynamoDbMatchEntity : DynamoDbEntity {
    var matchId: String

    var ttl: Long?
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class MatchInfoWrapper(
    @field:JsonAlias("match_info")
    @field:JsonProperty("matchInfo")
    var matchInfo: MatchInfo = MatchInfo()
)

@JsonIgnoreProperties(ignoreUnknown = true)
@DynamoDbBean
data class MatchInfo(
    @get:JsonIgnore
    @get:DynamoDbSecondaryPartitionKey(indexNames = [INDEX_ENTITY_NAME])
    override var entityName: String = ENTITY_NAME_MATCH_INFO,
    @field:JsonAlias("id")
    @field:JsonProperty("matchId")
    @get:DynamoDbPartitionKey
    @get:DynamoDbAttribute(PARTITION_KEY)
    //@DynamoDBTypeConverted(converter = MatchPrefixConverter::class)
    @get:DynamoDbConvertedBy(MatchPrefixConverter::class)
    override var matchId: String = "",
    @field:JsonProperty("scheduled")
    //@DynamoDBTypeConverted(converter = LocalDateTimeConverter::class)
    var scheduled: LocalDateTime? = null,
    @field:JsonAlias("start_time_tbd")
    @field:JsonProperty("startTimeTbd")
    var startTimeTbd: Boolean = false,
    @field:JsonProperty("season")
    var season: SeasonInfo? = null,
    @field:JsonAlias("status_info")
    @field:JsonProperty("statusInfo")
    var statusInfo: StatusInfo = StatusInfo(),
    @field:JsonProperty("competitors")
    var competitors: List<Competitor> = emptyList(),
    @field:JsonAlias("goal_scorer")
    @field:JsonProperty("goalScorer")
    var goalScorer: List<GoalScorer> = emptyList(),
    @field:JsonProperty("result")
    var result: MatchResult = MatchResult(),
    override var ttl: Long? = null
) : DynamoDbMatchEntity {
    @get:DynamoDbSortKey
    @get:DynamoDbAttribute(SORT_KEY)
    @get:DynamoDbConvertedBy(MatchPrefixConverter::class)
    @get:JsonIgnore
    var id: String
        get() = matchId
        set(value) {}
}

@JsonIgnoreProperties(ignoreUnknown = true)
@DynamoDbBean
data class SeasonInfo(
    @field:JsonProperty("id")
    var id: String = "",
    @field:JsonProperty("name")
    var name: String = "",
    @field:JsonProperty("startDate")
    //@DynamoDBTypeConverted(converter = LocalDateTimeConverter::class)
    var startDate: LocalDate? = null,
    @field:JsonProperty("endDate")
    //@DynamoDBTypeConverted(converter = LocalDateTimeConverter::class)
    var endDate: LocalDate? = null,
    @field:JsonAlias("year")
    @field:JsonProperty("season")
    var season: String? = null,
    @field:JsonAlias("tournament_id")
    @field:JsonProperty("tournamentId")
    var tournamentId: String = ""
)

@JsonIgnoreProperties(ignoreUnknown = true)
@DynamoDbBean
data class StatusInfo(
    @field:JsonProperty("status")
    var status: String = "",
    @field:JsonAlias("match_time")
    @field:JsonProperty("matchTime")
    var matchTime: String? = null,
    @field:JsonAlias("stoppage_time")
    @field:JsonProperty("stoppageTime")
    var stoppageTime: String? = null,
    @field:JsonProperty("timestamp")
    var timestamp: Long? = null,
    @field:JsonAlias("live_status")
    @field:JsonProperty("liveStatus")
    var liveStatus: String = ""
)

@JsonIgnoreProperties(ignoreUnknown = true)
@DynamoDbBean
data class Competitor(
    @field:JsonProperty("id")
    var id: String = "",
    @field:JsonProperty("name")
    var name: String = "",
    @field:JsonProperty("country")
    var country: String = "",
    @field:JsonAlias("country_code")
    @field:JsonProperty("countryCode")
    var countryCode: String = "",
    @field:JsonProperty("abbreviation")
    var abbreviation: String = "",
    @field:JsonProperty("qualifier")
    var qualifier: String = ""
)

@JsonIgnoreProperties(ignoreUnknown = true)
@DynamoDbBean
data class GoalScorer(
    @field:JsonProperty("id")
    var id: String = "",
    @field:JsonAlias("first_name")
    @field:JsonProperty("firstName")
    var firstName: String = "",
    @field:JsonAlias("last_name")
    @field:JsonProperty("lastname")
    var lastname: String = "",
    @field:JsonProperty("method")
    var method: String = "",
    @field:JsonAlias("match_time")
    @field:JsonProperty("matchTime")
    var matchTime: Int = 0,
    @field:JsonAlias("stoppage_time")
    @field:JsonProperty("stoppageTime")
    var stoppageTime: String = "",
    @field:JsonProperty("team")
    var team: String = ""
)

@JsonIgnoreProperties(ignoreUnknown = true)
@DynamoDbBean
data class MatchResult(
    @field:JsonProperty("status")
    var status: String = "",
    @field:JsonAlias("match_status")
    @field:JsonProperty("matchStatus")
    var matchStatus: String = "",
    @field:JsonAlias("home_score")
    @field:JsonProperty("homeScore")
    var homeScore: Int = 0,
    @field:JsonAlias("away_score")
    @field:JsonProperty("awayScore")
    var awayScore: Int = 0,
    @field:JsonAlias("winner_id")
    @field:JsonProperty("winnerId")
    var winnerId: String? = null,
    @field:JsonAlias("period_scores")
    @field:JsonProperty("periodScore")
    var periodScore: List<PeriodScore> = emptyList()
)

@JsonIgnoreProperties(ignoreUnknown = true)
@DynamoDbBean
data class PeriodScore(
    @field:JsonAlias("home_score")
    @field:JsonProperty("homeScore")
    var homeScore: Int = 0,
    @field:JsonAlias("away_score")
    @field:JsonProperty("awayScore")
    var awayScore: Int = 0,
    @field:JsonProperty("type")
    var type: String = "",
    @field:JsonProperty("number")
    var number: Int = 0
)