package at.ac.fhwn.bigdata.footballticker.service

import at.ac.fhwn.bigdata.footballticker.config.*
import at.ac.fhwn.bigdata.footballticker.entity.input.Match
import at.ac.fhwn.bigdata.footballticker.entity.input.MatchInfoWrapper
import at.ac.fhwn.bigdata.footballticker.entity.input.TimelineInfos
import at.ac.fhwn.bigdata.footballticker.repo.MatchEventAggregationRepo
import at.ac.fhwn.bigdata.footballticker.repo.MatchInfoRepo
import at.ac.fhwn.bigdata.footballticker.repo.TimelineInfoRepo
import at.ac.fhwn.bigdata.footballticker.repo.setTtlIfNotSet
import com.fasterxml.jackson.module.kotlin.readValue
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerRecord
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.time.temporal.ChronoUnit
import java.util.*
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit
import javax.annotation.PreDestroy

@RestController
@RequestMapping("/ticker")
class MatchService @Autowired constructor(
    private val tickerWebSocketService: TickerWebSocketService,
    private val matchInfoRepo: MatchInfoRepo,
    private val matchEventAggregationRepo: MatchEventAggregationRepo,
    private val timelineInfoRepo: TimelineInfoRepo
) {
    private val logger: Logger = LoggerFactory.getLogger(MatchService::class.qualifiedName)

    private val executorService = ThreadPoolExecutor(
        3, 3, 0L, TimeUnit.MILLISECONDS,
        LinkedBlockingQueue()
    )

    @PreDestroy
    fun destroy() {
        executorService.shutdown()
    }

    @GetMapping("all-match-infos")
    fun getAllMatchInfos() = matchInfoRepo.findAllMatchInfos()

    @GetMapping("all-match-event-aggregations")
    fun getAllMatchEventAggregations() = matchEventAggregationRepo.findAllMatchEventAggregations()

    @GetMapping("all-timeline-infos")
    fun getAllTimelineInfos() = timelineInfoRepo.findAllTimelineInfos()

    @GetMapping("match-event-aggregations")
    fun getMatchEventAggregations(@RequestParam("matchId") matchId: String) =
        matchEventAggregationRepo.findMatchEventAggregations(matchId)

    @GetMapping("timeline-infos")
    fun getTimelineInfos(@RequestParam("matchId") matchId: String) = timelineInfoRepo.findTimelineInfos(matchId)

    @DeleteMapping("/")
    fun deleteAllMatches() = matchInfoRepo.deleteAllMatchInfos()

    @DeleteMapping("/{matchId}")
    fun deleteMatch(@PathVariable("matchId") matchId: String) = matchInfoRepo.deleteMatch(matchId)

    @PostMapping("/produce")
    fun produceTestData() {
        val file = validateAndGetDataDirectory()
        val mapper = createJacksonMapper()
        val matchdayDirectories = (file.listFiles() ?: emptyArray()).filter { it.isDirectory }
        val matches = matchdayDirectories.flatMap { matchdayDir ->
            val matchDirectories = (matchdayDir.listFiles() ?: emptyArray()).filter { it.isDirectory }
            matchDirectories.mapNotNull { match ->
                val matchFiles = (match.listFiles() ?: emptyArray()).filter { it.isFile }
                val fileMap = matchFiles.filter { it.name.endsWith(".json") }
                    .associateBy { it.name.removeSuffix(".json") }
                val infoFile = fileMap["info"]
                val timelineFile = fileMap["timeline"]
                if (infoFile != null && timelineFile != null) {
                    val info = mapper.readValue<MatchInfoWrapper>(infoFile)
                    val timeline = mapper.readValue<TimelineInfos>(timelineFile)
                    Match(info.matchInfo, timeline.timelineInfos)
                } else {
                    null
                }
            }
        }
        sendMatchesToProducer(matches)
    }

    private fun sendMatchesToProducer(matches: List<Match>) {
        val mapper = createJacksonMapper()
        val props: Map<String, String> = mapOf(
            "bootstrap.servers" to KAFKA_ENDPOINT,
            "key.serializer" to
                    "org.apache.kafka.common.serialization.StringSerializer",
            "value.serializer" to
                    "org.apache.kafka.common.serialization.StringSerializer",
            "acks" to "all"
        )
        val producer = KafkaProducer<String, String>(props)
        producer.use { producer ->
            val futures = matches.map { match ->
                val future = executorService.submit {
                    try {
                        match.matchInfo.matchId = UUID.randomUUID().toString()
                        match.matchInfo.result.homeScore = 0
                        match.matchInfo.result.awayScore = 0
                        setTtlIfNotSet(match.matchInfo)
                        producer.send(
                            ProducerRecord(
                                TOPIC_MATCH,
                                match.matchInfo.matchId,
                                mapper.writeValueAsString(match.matchInfo)
                            )
                        )
                        match.timelineInfos.forEach {
                            it.matchId = match.matchInfo.matchId
                            it.id = UUID.randomUUID().toString()
                            it.period = it.period ?: 0
                            it.eventType = it.eventType?.ifEmpty { null } ?: "other"
                        }
                        val timelineInfos = match.timelineInfos.filter { it.time != null }.sortedBy { it.time }

                        timelineInfos.forEachIndexed { index, timelineInfo ->
                            if (index > 0) {
                                val seconds =
                                    ChronoUnit.SECONDS.between(timelineInfos[index - 1].time, timelineInfo.time)
                                Thread.sleep(seconds * 1000 / FAST_MOTION_FACTOR)
                            }
                            producer.send(
                                ProducerRecord(
                                    TOPIC_TICKER,
                                    timelineInfo.id,
                                    mapper.writeValueAsString(timelineInfo)
                                )
                            )
                            if (timelineInfo.eventType == "score_change") {
                                if (timelineInfo.team == "home") {
                                    match.matchInfo.result.homeScore++
                                } else {
                                    match.matchInfo.result.awayScore++
                                }
                                producer.send(
                                    ProducerRecord(
                                        TOPIC_MATCH,
                                        match.matchInfo.matchId,
                                        mapper.writeValueAsString(match.matchInfo)
                                    )
                                )
                            }
                        }
                    } catch (ex: Exception) {
                        logger.error("Error while sending match data to Kafka", ex)
                    }
                }
                Thread.sleep(SECONDS_BETWEEN_MATCH_STARTS * 1000L)
                future
            }
            futures.forEach { it.get() }
        }
    }

    companion object {
        const val PROPERTY_TICKER_DATA_PATH = "ticker.data.path"
        const val FAST_MOTION_FACTOR = 60
        const val SECONDS_BETWEEN_MATCH_STARTS = 10
    }
}
