package at.ac.fhwn.bigdata.footballticker.config

import at.ac.fhwn.bigdata.footballticker.service.MatchService
import org.springframework.web.server.ServerErrorException
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.dynamodb.DynamoDbClient
import java.io.File
import java.net.URI

const val KAFKA_ENDPOINT = "localhost:9092" // TODO move to properties file
const val DYNAMODB_ENDPOINT = "http://localhost:8000" // TODO move to properties file
const val S3_ENDPOINT = "http://localhost:9000" // TODO move to properties file
const val AWS_REGION = "ddblocal" // TODO move to properties file
const val STOMP_ENDPOINT = "ws://localhost:61616" // TODO move to properties file

const val TABLE_TICKER = "ticker"

const val TOPIC_MATCH = "match"
const val TOPIC_TICKER = "ticker"

fun validateAndGetDataDirectory(): File {
    val tickerDataPath = System.getProperty(MatchService.PROPERTY_TICKER_DATA_PATH)
    if (tickerDataPath.isNullOrEmpty()) {
        throw ServerErrorException("Property ${MatchService.PROPERTY_TICKER_DATA_PATH} not set")
    }
    val file = File(tickerDataPath)
    if (!file.exists() || !file.isDirectory || !file.canRead()) {
        throw ServerErrorException("Path $tickerDataPath is not a directory or can't be read.")
    }
    return file
}

fun dynamoDbClient(): DynamoDbClient {
    return DynamoDbClient.builder().endpointOverride(URI(DYNAMODB_ENDPOINT)).region(Region.of(AWS_REGION)).build()
}

fun dynamoDbEnhancedClient(): DynamoDbEnhancedClient {
    return DynamoDbEnhancedClient.builder().dynamoDbClient(dynamoDbClient()).build()
}
