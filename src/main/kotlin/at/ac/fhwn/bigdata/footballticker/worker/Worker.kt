package at.ac.fhwn.bigdata.footballticker.worker

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.concurrent.ExecutorService
import javax.annotation.PostConstruct
import javax.annotation.PreDestroy

abstract class Worker constructor(
    private var executor: ExecutorService,
) {
    private val logger: Logger = LoggerFactory.getLogger(Worker::class.qualifiedName)

    var running = false

    @PostConstruct
    fun startWorker() {
        running = true
        executor.submit { doWork() }
    }

    @PreDestroy
    fun stopWorker() {
        running = false
    }

    private fun doWork() {
        while (running) {
            try {
                workerLoop()
            } catch (ex: Exception) {
                logger.error("Error while executing worker", ex)
                Thread.sleep(1000)
            }
        }
    }

    abstract fun workerLoop()
}