package at.ac.fhwn.bigdata.footballticker.worker

import at.ac.fhwn.bigdata.footballticker.config.KAFKA_ENDPOINT
import at.ac.fhwn.bigdata.footballticker.config.TOPIC_MATCH
import at.ac.fhwn.bigdata.footballticker.config.TOPIC_TICKER
import at.ac.fhwn.bigdata.footballticker.config.createJacksonMapper
import at.ac.fhwn.bigdata.footballticker.entity.input.MatchInfo
import at.ac.fhwn.bigdata.footballticker.entity.input.TimelineInfo
import at.ac.fhwn.bigdata.footballticker.repo.MatchInfoRepo
import at.ac.fhwn.bigdata.footballticker.repo.TimelineInfoRepo
import com.fasterxml.jackson.module.kotlin.readValue
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.Duration
import java.util.concurrent.ExecutorService

@Service
class KafkaConsumer @Autowired constructor(
    executor: ExecutorService,
    private val matchInfoRepo: MatchInfoRepo,
    private val timelineInfoRepo: TimelineInfoRepo
) : Worker(executor) {
    private val objectMapper = createJacksonMapper()

    override fun workerLoop() {
        val stringDeserializer = "org.apache.kafka.common.serialization.StringDeserializer"
        val props: Map<String, String> = mapOf(
            "bootstrap.servers" to KAFKA_ENDPOINT,
            "key.deserializer" to stringDeserializer,
            "value.deserializer" to stringDeserializer,
            "acks" to "all",
            "group.id" to "app"
        )
        val consumer = KafkaConsumer<String, String>(props)
        consumer.subscribe(listOf(TOPIC_MATCH, TOPIC_TICKER))
        while (running) {
            val records = consumer.poll(Duration.ofSeconds(1))
            records.forEach {
                try {
                    processRecord(it)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
            Thread.sleep(10)
        }
    }

    fun processRecord(record: ConsumerRecord<String, String>) {
        when (record.topic()) {
            TOPIC_MATCH -> {
                val matchInfo = objectMapper.readValue<MatchInfo>(record.value())
                matchInfoRepo.save(matchInfo)
            }
            TOPIC_TICKER -> {
                val timelineInfo = objectMapper.readValue<TimelineInfo>(record.value())
                timelineInfoRepo.save(timelineInfo)
            }
        }
    }
}
