package at.ac.fhwn.bigdata.footballticker.entity.input

import at.ac.fhwn.bigdata.footballticker.config.MatchPrefixConverter
import at.ac.fhwn.bigdata.footballticker.config.PeriodEventConverter
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.*

const val PARTITION_KEY = "partitionKey"
const val SORT_KEY = "sortKey"

const val INDEX_ENTITY_NAME = "entityNameIndex"
const val ENTITY_NAME = "entityName"
const val ENTITY_NAME_MATCH_INFO = "matchInfo"
const val ENTITY_NAME_MATCH_TICKER_MESSAGE = "tickerMessage"
const val ENTITY_NAME_MATCH_AGGREGATION = "matchAggregation"

data class Match(
    var matchInfo: MatchInfo = MatchInfo(),
    var timelineInfos: List<TimelineInfo> = emptyList()
)

@JsonIgnoreProperties(ignoreUnknown = true)
@DynamoDbBean
data class MatchEventAggregation(
    @get:JsonIgnore
    @get:DynamoDbSecondaryPartitionKey(indexNames = [INDEX_ENTITY_NAME])
    override var entityName: String = ENTITY_NAME_MATCH_AGGREGATION,
    @get:DynamoDbPartitionKey
    @get:DynamoDbAttribute(PARTITION_KEY)
    @get:DynamoDbConvertedBy(MatchPrefixConverter::class)
    @field:JsonProperty("matchId")
    override var matchId: String = "",
    @field:JsonProperty("period")
    var period: Int? = null,
    @field:JsonProperty("eventType")
    var eventType: String? = null,
    @field:JsonProperty("eventCount")
    var eventCount: Int = 0,
    override var ttl: Long? = null
) : DynamoDbMatchEntity {
    @get:DynamoDbSortKey
    @get:DynamoDbAttribute(SORT_KEY)
    @get:DynamoDbConvertedBy(PeriodEventConverter::class)
    @get:JsonIgnore
    var periodEventType: MatchEventAggregationPeriodEvent
        get() = MatchEventAggregationPeriodEvent(period, eventType)
        set(value) {}
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class MatchEventAggregationPeriodEvent(
    @field:JsonProperty("period")
    var period: Int? = null,
    @field:JsonProperty("eventType")
    var eventType: String? = null
)