package at.ac.fhwn.bigdata.footballticker.service

import at.ac.fhwn.bigdata.footballticker.config.STOMP_ENDPOINT
import at.ac.fhwn.bigdata.footballticker.config.createJacksonMapper
import at.ac.fhwn.bigdata.footballticker.entity.input.DynamoDbEntity
import at.ac.fhwn.bigdata.footballticker.entity.input.ENTITY_NAME
import org.springframework.messaging.converter.StringMessageConverter
import org.springframework.messaging.simp.stomp.StompSession
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter
import org.springframework.stereotype.Service
import org.springframework.web.socket.client.WebSocketClient
import org.springframework.web.socket.client.standard.StandardWebSocketClient
import org.springframework.web.socket.messaging.WebSocketStompClient
import software.amazon.awssdk.services.dynamodb.model.OperationType
import javax.annotation.PostConstruct


@Service
class TickerWebSocketService {
    lateinit var stompSession: StompSession

    @PostConstruct
    fun createStompClient() {
        val client: WebSocketClient = StandardWebSocketClient()
        val stompClient = WebSocketStompClient(client)
        stompClient.messageConverter = StringMessageConverter()
        stompSession = stompClient.connect(STOMP_ENDPOINT, object : StompSessionHandlerAdapter() {}).get()
    }

    fun send(dynamoDbEntity: DynamoDbEntity) {
        send(dynamoDbEntity, OperationType.INSERT.name)
    }

    fun send(dynamoDbEntity: DynamoDbEntity, eventType: String) {
        val map = mapOf(
            "entity" to dynamoDbEntity,
            ENTITY_NAME to dynamoDbEntity.entityName,
            "eventType" to eventType
        )
        val objectMapper = createJacksonMapper()
        stompSession.send("/topic/ticker", objectMapper.writeValueAsString(map))
    }
}
