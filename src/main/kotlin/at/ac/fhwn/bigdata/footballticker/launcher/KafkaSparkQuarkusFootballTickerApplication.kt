package at.ac.fhwn.bigdata.footballticker.launcher

import at.ac.fhwn.bigdata.footballticker.config.AWS_REGION
import at.ac.fhwn.bigdata.footballticker.config.DYNAMODB_ENDPOINT
import at.ac.fhwn.bigdata.footballticker.config.validateAndGetDataDirectory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import java.util.concurrent.ExecutorService
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit


@SpringBootApplication
@ComponentScan("at.ac.fhwn.bigdata.footballticker")
class KafkaSparkQuarkusFootballTickerApplication {
    @Bean
    fun executorService(): ExecutorService {
        return ThreadPoolExecutor(
            10, 10, 0L, TimeUnit.MILLISECONDS,
            LinkedBlockingQueue()
        )
    }
}

fun main(args: Array<String>) {
    System.setProperty("spring.devtools.restart.enabled", "false");
    System.setProperty("aws.dynamodb.region", AWS_REGION)
    System.setProperty("aws.dynamodb.endpoint", DYNAMODB_ENDPOINT)
    System.setProperty("aws.accessKeyId", "local")
    System.setProperty("aws.secretKey", "local")
    //System.setProperty("software.amazon.awssdk.http.service.impl", "software.amazon.awssdk.http.SdkHttpService")
    System.setProperty("software.amazon.awssdk.http.service.impl", "software.amazon.awssdk.http.apache.ApacheSdkHttpService")
    validateAndGetDataDirectory()
    runApplication<KafkaSparkQuarkusFootballTickerApplication>(*args)
}
