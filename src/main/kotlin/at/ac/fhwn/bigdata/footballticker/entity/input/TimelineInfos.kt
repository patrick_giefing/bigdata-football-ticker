package at.ac.fhwn.bigdata.footballticker.entity.input

import at.ac.fhwn.bigdata.footballticker.config.MatchPrefixConverter
import at.ac.fhwn.bigdata.footballticker.config.MessagePrefixConverter
import com.fasterxml.jackson.annotation.JsonAlias
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.*
import java.time.LocalDateTime

@JsonIgnoreProperties(ignoreUnknown = true)
data class TimelineInfos(
    @field:JsonAlias("timeline_info")
    @field:JsonProperty("timelineInfos")
    var timelineInfos: List<TimelineInfo> = emptyList()
)

@JsonIgnoreProperties(ignoreUnknown = true)
@DynamoDbBean
data class TimelineInfo(
    @get:JsonIgnore
    @get:DynamoDbSecondaryPartitionKey(indexNames = [INDEX_ENTITY_NAME])
    override var entityName: String = ENTITY_NAME_MATCH_TICKER_MESSAGE,
    @get:DynamoDbPartitionKey
    @get:DynamoDbAttribute(PARTITION_KEY)
    @get:DynamoDbConvertedBy(MatchPrefixConverter::class)
    @field:JsonProperty("matchId")
    override var matchId: String = "",
    @get:DynamoDbSortKey
    @get:DynamoDbAttribute(SORT_KEY)
    @get:DynamoDbConvertedBy(MessagePrefixConverter::class)
    @field:JsonProperty("id")
    var id: String = "",
    @field:JsonProperty("text")
    var text: String? = null,
    @field:JsonProperty("team")
    var team: String? = null,
    @field:JsonProperty("title")
    var title: String? = null,
    @field:JsonAlias("type")
    @field:JsonProperty("eventType")
    var eventType: String? = null,
    @field:JsonProperty("time")
    //@DynamoDBTypeConverted(converter = LocalDateTimeConverter::class)
    var time: LocalDateTime? = null,
    @field:JsonProperty("period")
    var period: Int? = null,
    @field:JsonAlias("match_time")
    @field:JsonProperty("matchTime")
    var matchTime: String? = null,
    @field:JsonAlias("period_type")
    @field:JsonProperty("periodType")
    var periodType: String? = null,
    @field:JsonAlias("stoppage_time")
    @field:JsonProperty("stoppageTime")
    var stoppageTime: String? = null,
    @field:JsonProperty("player")
    var player: Player? = null,
    @field:JsonAlias("player_in")
    @field:JsonProperty("playerIn")
    var playerIn: Player? = null,
    @field:JsonAlias("player_out")
    @field:JsonProperty("playerOut")
    var playerOut: Player? = null,
    override var ttl: Long? = null
) : DynamoDbMatchEntity

@JsonIgnoreProperties(ignoreUnknown = true)
@DynamoDbBean
data class Player(
    @field:JsonProperty("id")
    var id: String = "",
    @field:JsonProperty("name")
    var name: String = ""
)