package at.ac.fhwn.bigdata.footballticker.service

import at.ac.fhwn.bigdata.footballticker.config.KAFKA_ENDPOINT
import at.ac.fhwn.bigdata.footballticker.config.TOPIC_TICKER
import at.ac.fhwn.bigdata.footballticker.entity.input.MatchEventAggregation
import at.ac.fhwn.bigdata.footballticker.repo.matchEventAggregationRepoInstance
import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.spark.sql.*
import org.apache.spark.sql.streaming.OutputMode
import org.apache.spark.sql.streaming.Trigger
import org.apache.spark.sql.types.DataTypes
import org.apache.spark.sql.types.StructType
import org.springframework.stereotype.Service
import javax.annotation.PostConstruct

@Service
class SparkTickerAggregator {
    @PostConstruct
    fun init() {
        createSparkContext()
    }

    fun createSparkContext() {
        val sparkSession = SparkSession.builder()
            .appName("football-ticker")
            .master("local[*]")
            .config("spark.executor.cores", 1)
            .config("spark.driver.cores", 1)
            .orCreate

        val schema = StructType()
            .add("matchId", DataTypes.StringType, false)
            .add("period", DataTypes.IntegerType, true)
            .add("eventType", DataTypes.StringType, false)

        val tickerDataset = sparkSession.readStream()
            .format("kafka")
            .option("kafka.bootstrap.servers", KAFKA_ENDPOINT)
            .option("subscribe", TOPIC_TICKER)
            .option("startingOffsets", "latest")
            .load()
            .selectExpr("topic", "timestamp", "partition", "offset", "CAST(key AS STRING)", "CAST(value AS STRING)")
            .select(functions.from_json(Column("value"), schema).alias("values"))
            .select("values.*")
        val dfName = "df_ticker_${System.currentTimeMillis()}"
        tickerDataset.createTempView(dfName)
        tickerDataset.sqlContext()
            .sql("select matchId, period, eventType, count(*) as eventCount from $dfName group by matchId, period, eventType")
            .writeStream().foreach(MatchEventAggregationWriter()).outputMode(OutputMode.Update())
            .trigger(Trigger.ProcessingTime(0))
            .start()
    }
}

class MatchEventAggregationWriter : ForeachWriter<Row>() {
    override fun open(partitionId: Long, version: Long): Boolean {
        return true
    }

    override fun process(row: Row) {
        try {
            val objectMapper = ObjectMapper()
            val matchEventAggregation =
                objectMapper.readValue(row.json(), MatchEventAggregation::class.java)
            matchEventAggregationRepoInstance.save(matchEventAggregation)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun close(errorOrNull: Throwable?) {
        errorOrNull?.printStackTrace()
    }
}