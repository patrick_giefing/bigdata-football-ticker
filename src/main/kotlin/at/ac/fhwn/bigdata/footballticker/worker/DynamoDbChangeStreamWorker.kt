package at.ac.fhwn.bigdata.footballticker.worker

import at.ac.fhwn.bigdata.footballticker.config.AWS_REGION
import at.ac.fhwn.bigdata.footballticker.config.DYNAMODB_ENDPOINT
import at.ac.fhwn.bigdata.footballticker.config.TABLE_TICKER
import at.ac.fhwn.bigdata.footballticker.config.dynamoDbClient
import at.ac.fhwn.bigdata.footballticker.entity.input.*
import at.ac.fhwn.bigdata.footballticker.repo.TimelineInfoRepo
import at.ac.fhwn.bigdata.footballticker.service.S3Service
import at.ac.fhwn.bigdata.footballticker.service.TickerWebSocketService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import software.amazon.awssdk.enhanced.dynamodb.TableSchema
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.dynamodb.model.*
import software.amazon.awssdk.services.dynamodb.streams.DynamoDbStreamsClient
import java.net.URI
import java.util.concurrent.ExecutorService

@Service
class DynamoDbChangeStreamWorker @Autowired constructor(
    executor: ExecutorService,
    private val tickerWebSocketService: TickerWebSocketService,
    private val s3Service: S3Service,
    private val tickerRepo: TimelineInfoRepo
) : Worker(executor) {
    private val logger: Logger = LoggerFactory.getLogger(DynamoDbChangeStreamWorker::class.qualifiedName)

    override fun workerLoop() {
        val dynamoDbClient = dynamoDbClient()
        val describeTableResult = dynamoDbClient.describeTable(DescribeTableRequest.builder().tableName(TABLE_TICKER).build())
        val streamArn = describeTableResult.table().latestStreamArn()

        var lastEvaluatedShardId: String? = null

        while (running) {
            val streamsClient = DynamoDbStreamsClient.builder().endpointOverride(URI(DYNAMODB_ENDPOINT)).region(Region.of(AWS_REGION)).build()
            do {
                val describeStreamResult = streamsClient.describeStream(DescribeStreamRequest.builder().streamArn(streamArn).exclusiveStartShardId(lastEvaluatedShardId).build())
                val shards = describeStreamResult.streamDescription().shards()

                for (shard in shards) {
                    val shardId = shard.shardId()
                    logger.debug("Shard: $shard")

                    val getShardIteratorRequest = GetShardIteratorRequest.builder()
                        .streamArn(streamArn)
                        .shardId(shardId)
                        .shardIteratorType(ShardIteratorType.TRIM_HORIZON)
                        .build()

                    val getShardIteratorResult = streamsClient.getShardIterator(getShardIteratorRequest)
                    var currentShardIter = getShardIteratorResult.shardIterator()

                    while (currentShardIter != null && running) {
                        logger.debug("    Shard iterator: $currentShardIter")

                        val getRecordsResult = streamsClient.getRecords(GetRecordsRequest.builder().shardIterator(currentShardIter).build())
                        val records = getRecordsResult.records()
                        for (record in records) {
                            try {
                                handleRecord(record)
                            } catch (ex: Exception) {
                                logger.error("Error while handling record", ex)
                            }
                        }
                        currentShardIter = getRecordsResult.nextShardIterator()
                        Thread.sleep(10)
                    }
                }

                lastEvaluatedShardId = describeStreamResult.streamDescription().lastEvaluatedShardId()
                Thread.sleep(10)
            } while (lastEvaluatedShardId != null && running)
            Thread.sleep(10)
        }
    }

    private fun handleRecord(record: Record) {
        when (record.eventName()) {
            OperationType.REMOVE -> convertToEntity(record.dynamodb().oldImage())?.let {
                if (it is MatchInfo) {
                    tickerWebSocketService.send(
                        it,
                        record.eventName().name
                    )
                    s3Service.saveMatch(
                        Match(
                            matchInfo = it,
                            timelineInfos = tickerRepo.findTimelineInfos(it.matchId)
                        )
                    )
                }
            }
            OperationType.MODIFY, OperationType.INSERT -> convertToEntity(record.dynamodb().newImage())?.let {
                tickerWebSocketService.send(
                    it,
                    record.eventName().name
                )
            }
            else -> logger.debug("Unhandled eventName ${record.eventName()}")
        }
    }

    private fun convertToEntity(valueMap: Map<String, AttributeValue>): DynamoDbMatchEntity? {
        if (!valueMap.containsKey(ENTITY_NAME)) return null
        val entityName = valueMap[ENTITY_NAME]!!
        return when (entityName.s()) {
            ENTITY_NAME_MATCH_INFO -> TableSchema.fromClass(MatchInfo::class.java).mapToItem(valueMap)
            ENTITY_NAME_MATCH_TICKER_MESSAGE -> TableSchema.fromClass(TimelineInfo::class.java).mapToItem(valueMap)
            ENTITY_NAME_MATCH_AGGREGATION -> TableSchema.fromClass(MatchEventAggregation::class.java).mapToItem(valueMap)
            else -> null
        }
    }
}