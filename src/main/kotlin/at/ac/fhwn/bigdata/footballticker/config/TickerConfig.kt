package at.ac.fhwn.bigdata.footballticker.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

fun createJacksonMapper(): ObjectMapper {
    val mapper = jacksonObjectMapper()
    val module = JavaTimeModule()
    val deserializer = LocalDateTimeDeserializer(DateTimeFormatter.ISO_OFFSET_DATE_TIME)
    module.addDeserializer(LocalDateTime::class.java, deserializer)
    mapper.registerModule(module)
    mapper.registerKotlinModule()
    return mapper
}