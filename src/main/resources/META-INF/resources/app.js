let stompClient = null;

$(document).ready(function () {
    $.ajax("./ticker/all-match-infos").done(function (data) {
        data.forEach(match => addMatch(match));
    });

    $('.load-test-data').click(() => {
        $.post("./ticker/produce", () => {
        });
    });
});

function addMatch(match) {
    if (validateMatch(match)) {
        const matchesPanel = $('div#matches')
        let matchPane = matchesPanel.find(`div.match[match-id=${match.matchId}]`)
        if (matchPane.length === 0) {
            matchPane = createMatchPane(match);
            matchesPanel.append(matchPane);
            fadeIn(matchPane);
        }
        matchPane.find('.team.home .goals').html(match.result.homeScore);
        matchPane.find('.team.away .goals').html(match.result.awayScore);
        console.info("added match", match);
    } else {
        console.warn("ignored match because validations failed", match);
    }
}

function removeMatch(matchId) {
    const matchContainers = $(`[match-id=${matchId}]`);
    fadeOutAndRemove(matchContainers);
}

function createMatchPane(match) {
    const home = match.competitors[0];
    const away = match.competitors[1];
    const matchPane = $(`<div class="match" match-id="${match.matchId}">
                <span class="team home ${home.countryCode} ${home.abbreviation}"><span class="icon"></span><span class="name">${home.name}</span><span class="goals"></span></span> 
                <span class="team away ${away.countryCode} ${away.abbreviation}"><span class="icon"></span><span class="name">${away.name}</span><span class="goals"></span></span>
              </div>`)
    matchPane.click(() => {
        showMatch(match.matchId)
        $('.match.active').removeClass("active");
        matchPane.addClass("active");
    });
    return matchPane;
}

function showMatch(matchId) {
    loadMatchSummary(matchId)
    loadTicker(matchId)
}

function loadMatchSummary(matchId) {
    $('div#match-summary').empty().append($(`<div match-id="${matchId}"></div>`));
    $.ajax(`./ticker/match-event-aggregations?matchId=${matchId}`).done(function (data) {
        data.forEach(matchSummary => addMatchSummary(matchSummary));
    });
}

function addMatchSummary(matchSummary) {
    const matchSummaryPanel = $(`div#match-summary div[match-id=${matchSummary.matchId}]`);
    let matchSummaryPane = matchSummaryPanel.find(`div[event-type=${matchSummary.eventType}]`)
    if (matchSummaryPane.length === 0) {
        matchSummaryPane = $(`<div event-type="${matchSummary.eventType}" class="event-summary">
                                    <span class="eventType ${matchSummary.eventType}"></span>
                                    <table class="periodCounts">
                                        <tr class="1st">
                                            <td class="label">1. Hz</td>
                                            <td class="value"></td>
                                        </tr>
                                        <tr class="2nd">
                                            <td class="label">2. Hz</td>
                                            <td class="value"></td>
                                        </tr>
                                        <tr class="rest">
                                            <td class="label">Sonstiges</td>
                                            <td class="value"></td>
                                        </tr>
                                    </table>
                                </div>`);
        matchSummaryPanel.append(fadeIn(matchSummaryPane));
    }
    let periodValueCell;
    if (matchSummary.period === 1) {
        periodValueCell = matchSummaryPane.find('tr.1st td.value');
    } else if (matchSummary.period === 2) {
        periodValueCell = matchSummaryPane.find('tr.2nd td.value');
    } else {
        periodValueCell = matchSummaryPane.find('tr.rest td.value');
    }
    periodValueCell.html(matchSummary.eventCount);
}

function fadeIn(el) {
    el.css("display", "none").css("background-color", "lightseagreen").fadeIn(1000, () => {
        el.css("background-color", "white");
    });
    return el;
}

function fadeOutAndRemove(el) {
    el.css("background-color", "coral").fadeOut(2000, () => el.remove())
}

function loadTicker(matchId) {
    $('div#ticker').empty().append(`<div class="ticker" match-id="${matchId}"></div>`);
    $.ajax(`./ticker/timeline-infos?matchId=${matchId}`).done(function (data) {
        data.reverse().forEach(tickerMessage => addTickerMessage(tickerMessage));
    });
}

function addTickerMessage(tickerMessage) {
    const matchSummaryPanel = $(`div#ticker div[match-id=${tickerMessage.matchId}]`);
    let tickerMessagePane = matchSummaryPanel.find(`div[message-id=${tickerMessage.id}]`)
    if (tickerMessagePane.length > 0) return; // message already existing, update doesn't make sense
    tickerMessagePane = $(`<div class="message" message-id="${tickerMessage.id}"><span class="eventType tiny ${tickerMessage.eventType}"></span><span class="text">${tickerMessage.text}</span></div>`);
    matchSummaryPanel.prepend(fadeIn(tickerMessagePane));
}

function validateMatch(match) {
    if (!match.competitors || match.competitors.length != 2) return false;
    return true;
}

function connect() {
    const stompClient = Stomp.client("ws://127.0.0.1:61616");
    stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/ticker', function (message) {
            handleWsMessage(JSON.parse(message.body));
        });
    });
}

function handleWsMessage(message) {
    const entityName = message.entityName;
    const eventType = message.eventType;
    const entity = message.entity;
    if (!entityName || !eventType || !entity) {
        console.warn("invalid websocket message", message)
        return;
    }
    const matchId = entity.matchId;
    if (!matchId) {
        console.warn("entity doesn't contain a matchId", entity);
        return;
    }
    if (eventType == "REMOVE") {
        console.info("removing item");
        removeMatch(matchId);
    } else {
        console.info("adding item");
        if (entityName == "matchInfo") {
            addMatch(entity);
        } else if (entityName == "tickerMessage") {
            addTickerMessage(entity);
        } else if (entityName == "matchAggregation") {
            addMatchSummary(entity);
            signalChangeInMatchEvent(matchId, entity.eventType);
        } else {
            console.warn("invalid entityName", entityName)
        }
        signalChangeInMatch(matchId);
    }
}

function signalChangeInMatchEvent(matchId, eventType) {
    signalChange($(`div#match-summary div[match-id=${matchId}] div[event-type=${eventType}]`));
}

function signalChangeInMatch(matchId) {
    signalChange($(`.match[match-id=${matchId}]`));
}

function signalChange(el) {
    el.css("border-color", "red").css("border-width", "5px").css("padding", "-3px")
        .delay(500)
        .queue(function (next) {
            $(this).css("border-color", "").css("border-width", "").css("padding", "")
            next();
        });
}

connect();