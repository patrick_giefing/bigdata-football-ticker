package at.ac.fhwn.bigdata.footballticker.entity.input.test

import at.ac.fhwn.bigdata.footballticker.config.createJacksonMapper
import at.ac.fhwn.bigdata.footballticker.entity.input.MatchEventAggregation
import at.ac.fhwn.bigdata.footballticker.entity.input.MatchEventAggregationPeriodEvent
import at.ac.fhwn.bigdata.footballticker.entity.input.MatchInfoWrapper
import at.ac.fhwn.bigdata.footballticker.entity.input.TimelineInfos
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.io.File

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TimelineInfosSerializerTest {
    lateinit var mapper: ObjectMapper

    @BeforeAll
    fun before() {
        this.mapper = createJacksonMapper()
    }

    @Test
    fun deserializeTimelineInfos() {
        val timelineInfos =
            mapper.readValue<TimelineInfos>(File("C:\\dev\\projects\\kafka-spark-quarkus-football-ticker\\data\\17. Runde\\LASK-Ried\\timeline.json"))
        Assertions.assertTrue(timelineInfos.timelineInfos.isNotEmpty())
    }

    @Test
    fun deserializeInfo() {
        val matchInfoWrapper =
            mapper.readValue<MatchInfoWrapper>(File("C:\\dev\\projects\\kafka-spark-quarkus-football-ticker\\data\\17. Runde\\LASK-Ried\\info.json"))
        Assertions.assertTrue(matchInfoWrapper.matchInfo.matchId.isNotEmpty())
    }

    @Test
    fun serializeSummary() {
        val summary = MatchEventAggregation(
            matchId = "123",
            period = 1,
            eventType = "info",
            eventCount = 100
        )
        val json = mapper.writeValueAsString(summary)
        val openingCurlyBracesCount = json.filter { it == '{' }.count()
        Assertions.assertEquals(1, openingCurlyBracesCount)
        val summaryDeserialized = mapper.readValue(json, MatchEventAggregation::class.java)
        Assertions.assertEquals(summary, summaryDeserialized)
    }
}