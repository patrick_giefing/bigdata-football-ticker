package at.ac.fhwn.bigdata.footballticker.s3.test

import at.ac.fhwn.bigdata.footballticker.entity.input.Match
import at.ac.fhwn.bigdata.footballticker.entity.input.MatchInfo
import at.ac.fhwn.bigdata.footballticker.service.S3Service
import org.junit.jupiter.api.Test

class S3Test {
    @Test
    fun s3Test() {
        S3Service().saveMatch(Match(matchInfo = MatchInfo(matchId = "test")))
    }
}