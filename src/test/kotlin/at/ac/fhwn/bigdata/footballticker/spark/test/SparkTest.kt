package at.ac.fhwn.bigdata.footballticker.spark.test

import at.ac.fhwn.bigdata.footballticker.service.MatchService
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.junit.jupiter.api.Test
import java.time.Duration

class SparkTest {
    @Test
    fun testSpark() {
        /*val matchService = MatchService()
        matchService.init()
        Thread.sleep(3_600_000)*/
    }

    @Test
    fun testConsumer() {
        val props: Map<String, String> = mapOf(
            "bootstrap.servers" to "localhost:9092",
            "key.deserializer" to
                    "org.apache.kafka.common.serialization.StringDeserializer",
            "value.deserializer" to
                    "org.apache.kafka.common.serialization.StringDeserializer",
            "acks" to "all",
            "group.id" to "unittest"
        )
        val consumer = KafkaConsumer<String, String>(props)
        consumer.subscribe(listOf("match"))
        while (true) {
            val records = consumer.poll(Duration.ofSeconds(1))
            records.forEach {
                print(it.value())
            }
        }
    }

    @Test
    fun testProducer() {
        //MatchService().produceTestData()
    }
}