package at.ac.fhwn.bigdata.footballticker.dynamodb.test

import at.ac.fhwn.bigdata.footballticker.entity.input.MatchEventAggregation
import at.ac.fhwn.bigdata.footballticker.entity.input.MatchInfo
import at.ac.fhwn.bigdata.footballticker.entity.input.TimelineInfo
import at.ac.fhwn.bigdata.footballticker.repo.MatchEventAggregationRepo
import at.ac.fhwn.bigdata.footballticker.repo.MatchInfoRepo
import at.ac.fhwn.bigdata.footballticker.repo.TimelineInfoRepo
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import software.amazon.awssdk.enhanced.dynamodb.TableSchema
import java.time.LocalDateTime
import java.util.*

class DynamoDbTest {
    @Test
    fun testRepo() {
        System.setProperty("software.amazon.awssdk.http.service.impl", "software.amazon.awssdk.http.apache.ApacheSdkHttpService")
        val tickerRepo = TimelineInfoRepo()
        val matchEventAggregationRepo = MatchEventAggregationRepo()
        val matchRepo = MatchInfoRepo(matchEventAggregationRepo, tickerRepo)

        matchEventAggregationRepo.save(MatchEventAggregation().copy(matchId = "match-1", eventType = "1", period = 1))
        tickerRepo.save(TimelineInfo().copy(matchId = "match-1", id = "entry-1"))
        tickerRepo.save(TimelineInfo().copy(matchId = "match-1", id = "entry-2"))
        matchRepo.save(MatchInfo().copy(matchId = "match-1", scheduled = LocalDateTime.now()))

        matchEventAggregationRepo.save(MatchEventAggregation().copy(matchId = "match-2", eventType = "1", period = 1))
        tickerRepo.save(TimelineInfo().copy(matchId = "match-2", id = "entry-1"))
        tickerRepo.save(TimelineInfo().copy(matchId = "match-2", id = "entry-2"))
        matchRepo.save(MatchInfo().copy(matchId = "match-2", scheduled = LocalDateTime.now()))

        val matchInfos = matchRepo.findAllMatchInfos()
        Assertions.assertTrue(matchInfos.isNotEmpty())

        matchRepo.deleteMatch("match-1")
    }

    @Test
    fun convertObject() {
        val matchInfo = MatchInfo(matchId = UUID.randomUUID().toString(), ttl = System.currentTimeMillis())
        val schema = TableSchema.fromClass(MatchInfo::class.java)
        val map = schema.itemToMap(matchInfo, true)
        val matchInfoRecreated = schema.mapToItem(map)
        Assertions.assertEquals(matchInfo, matchInfoRecreated)
    }
}